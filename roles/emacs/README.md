# emacs_ansible

### Premise
.emacs.dをgitからcloneするためにbitbucketへの登録が必要となる。
必要のない場合はemacs/tasks/main.ymlのgit cloneをアンコメントすること。
git cloneを無視する場合は設定は不要である。

なお、.emacs.dは

* [makecolors/.emacs.d](https://bitbucket.org/makecolors/.emacs.d)
を利用する。

従って、ansibleのgit cloneに必要になるため、以下の設定が必要となる

* bitbucketへのアカウント登録
* ansibleを実行するサーバにおける公開鍵と秘密鍵の作成
* bitbucketへのSSH-Key(公開鍵)の登録

### edit Vagrantfile
```
------------------- Vagrantfile ------------------
# If true, then any SSH connections made will enable agent forwarding.
# Default value: false
config.ssh.forward_agent = true
--------------------------------------------------
vagrant up
```
### edit ansible.cfg
```
sudo vi emacs /etc/ansible/ansible.cfg
------------------ ansible.cfg -------------------
[ssh_connection]
ssh_args = -o ForwardAgent=yes
--------------------------------------------------
```
### set-up ssh-agent
```
ssh-agent
eval $(ssh-agent)
ssh-add ~/.ssh/id_rsa #秘密鍵の登録
ssh-add -l # 秘密鍵の確認
> 2048 xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx .ssh/id_rsa (RSA)
ssh -A xxxx@yyyyyy # -Aオプションはssh先へ情報を引き継ぐ
```

### reference
* [AnsibleからVagrantにssh経由でgit cloneする時の注意点](http://qiita.com/kilhyungdoo/items/fe6e1aca97891ac8fbdf)
* [ssh-add がCould not open a connection to your authentication agent.と出て成功しないとき](http://qiita.com/yuzuhara/items/85bee6767d4f562120b5)
* [Ansible で git clone させる](http://qiita.com/seizans/items/f5f052aec1592c47767f)