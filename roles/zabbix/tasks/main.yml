---

- name: check argment mode
  fail: msg='you must add `--extra-vars "mode=[ server | agentd ]"` in the train of ansible command.'
  when: mode is undefined

- name: check install zabbix
  command: which /usr/local/build/zabbix-{{ zabbix_version }}/bin/zabbix_get
  register: installed
  ignore_errors: True

- name: untar zabbix tarball
  unarchive:
    src={{ zabbix_tarball_file }}
    dest=/usr/local/src
  become: yes
  when: installed | failed

- name: install required packages in zabbix
  yum: name={{ item }} state=latest
  with_items:
    - net-snmp-devel.x86_64
    - libcurl-devel.x86_64
    - gcc
    - MySQL-python
  become: yes

- name: create group zabbix
  group: name=zabbix
  become: yes
   
- name: create user zabbix
  user:
    name=zabbix
    group=zabbix
  become: yes

- name: configure zabbix_server source
  command: >
    chdir={{ zabbix_src_path }}
    ./configure --enable-server --enable-agent --with-mysql=/usr/local/mysql/bin/mysql_config --enable-ipv6 --with-net-snmp --with-libcurl --with-libxml2 --prefix=/usr/local/build/zabbix-{{ zabbix_version }}
  become: yes
  when:
    - installed | failed
    - mode == 'server'

- name: configure zabbix_agentd source
  command: >
    chdir={{ zabbix_src_path }}
    ./configure --enable-agent --prefix=/usr/local/build/zabbix-{{ zabbix_version }}
  become: yes
  when:
    - installed | failed
    - mode == 'agentd'

- name: make install zabbix
  command: >
    chdir={{ zabbix_src_path }}
    make install
  become: yes
  when: installed | failed

- name: set zabbix symbolic link
  command: ln -snf /usr/local/build/zabbix-{{ zabbix_version }} /usr/local/zabbix
  become: yes
  when: installed | failed

- name: distribute zabbix_server.conf
  template:
    src=zabbix_server.conf
    dest=/usr/local/zabbix/etc/
  become: yes
  when:
    - mode == 'server'
    
- name: distribute zabbix_agentd.conf
  template:
    src=zabbix_agentd.conf
    dest=/usr/local/zabbix/etc/
  become: yes

- name: boot daemon mysqld
  command: service mysqld start
  become: yes
  when:
    - mode == 'server'
    
- name: create zabbix database
  mysql_db:
    name={{ zabbix_database }}
    encoding=utf8
    collation=utf8_bin
    login_user=root
    login_password=''
    login_unix_socket={{ mysql_socket_path }}
  become: yes
  when:
    - mode == 'server'
    
- name: create user
  mysql_user:
    name={{ zabbix_user }}
    password={{ zabbix_password }}
    priv={{ zabbix_database }}.*:ALL,GRANT
    login_user=root
    login_password=''
    login_unix_socket={{ mysql_socket_path }}
  when:
    - mode == 'server'
    
- name: import schema.sql to zabbix database
  shell:
    chdir={{ zabbix_src_path }}/database/mysql
    creates={{ zabbix_build_path }}/schema.done
    {{ mysql_command }} -u '{{ zabbix_user }}' -p'{{ zabbix_password }}' -D '{{ zabbix_database }}' < schema.sql && touch {{ zabbix_build_path }}/schema.done
  become: yes
  when:
    - mode == 'server'
    
- name: import images.sql to zabbix database
  shell:
    chdir={{ zabbix_src_path }}/database/mysql
    creates={{ zabbix_build_path }}/images.done
    {{ mysql_command }} -u '{{ zabbix_user }}' -p'{{ zabbix_password }}' -D '{{ zabbix_database }}' < images.sql && touch {{ zabbix_build_path }}/images.done
  become: yes
  when:
    - mode == 'server'
    
- name: import data.sql to zabbix database
  shell: 
    chdir={{ zabbix_src_path }}/database/mysql
    creates={{ zabbix_build_path }}/data.done
    {{ mysql_command }} -u '{{ zabbix_user }}' -p'{{ zabbix_password }}' -D '{{ zabbix_database }}' < data.sql && touch {{ zabbix_build_path }}/data.done
  become: yes
  when:
    - mode == 'server'

- name: set post_max_size in php.ini
  lineinfile:
    dest={{ phpini_lnpath }}
    regexp='^post\_max\_size'
    line='post_max_size = 16M'
  become: yes
  when:
    - mode == 'server'

- name: set max_execution_time in php.ini
  lineinfile:
    dest={{ phpini_lnpath }}
    regexp='^max\_execution\_time'
    line='max_execution_time = 300'
  become: yes
  when:
    - mode == 'server'

- name: set max_input_time in php.ini
  lineinfile:
    dest={{ phpini_lnpath }}
    regexp='^max\_input\_time'
    line='max_input_time = 300'
  become: yes
  when:
    - mode == 'server'

- name: set always_populate_raw_post_data in php.ini
  lineinfile:
    dest={{ phpini_lnpath }}
    regexp='^\;always\_populate\_raw\_post\_data'
    line='always_populate_raw_post_data = -1'
  become: yes
  when:
    - mode == 'server'

    
- name: create zabbix httpd directory
  file: path={{ htdocs_lnpath }} state=directory
  become: yes
  when:
    - mode == 'server'
    
- name: zabbix frontend copy
  shell:
    creates={{ htdocs_lnpath }}/frontend.done
    cp -a {{ zabbix_src_path }}/frontends/php/. {{ htdocs_lnpath }} && touch {{ htdocs_lnpath }}/frontend.done
  become: yes
  when:
    - mode == 'server'

- name: distribute zabbix_agentd.conf
  template:
    src=zabbix.conf.php
    dest={{ htdocs_lnpath }}/conf
  become: yes
  when:
    - mode == 'server'

- name: copy zabbix_server service file
  shell:
    cp {{ zabbix_src_path }}/misc/init.d/fedora/core5/zabbix_server /etc/init.d/zabbix_server
  become: yes
  when:
    - mode == 'server'
    
- name: copy zabbix_agentd service file
  shell:
    cp {{ zabbix_src_path }}/misc/init.d/fedora/core5/zabbix_agentd /etc/init.d/zabbix_agentd
  become: yes

- name: editing zabbix_server
  lineinfile:
    dest=/etc/init.d/zabbix_server
    regexp='^ZABBIX\_BIN\='
    line='ZABBIX_BIN="{{ zabbix_build_path }}/sbin/zabbix_server"'
  become: yes
  when:
    - mode == 'server'
    
- name: editing zabbix_agentd
  lineinfile:
    dest=/etc/init.d/zabbix_agentd
    regexp='^ZABBIX\_BIN\='
    line='ZABBIX_BIN="{{ zabbix_build_path }}/sbin/zabbix_agentd"'
  become: yes

- name: add zabbix_server service
  command: chkconfig --add zabbix_server
  become: yes
  when:
    - mode == 'server'
    
- name: add zabbix_agentd service
  command: chkconfig --add zabbix_agentd
  become: yes
