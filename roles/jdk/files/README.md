# download
java development kitをこのディレクトリ内にダウンロードする
ここではjavascriptによる制限のためwgetを使えないのでURLのみ記載する

jdk 7: http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html
jdk 8: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

ダウンロード例
ex. jdk-7u45-linux-x64.tar.gz