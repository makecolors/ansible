#! /bin/bash

# this program must be used after being distributed slaves files
echo "this program must be used by user(:vagrant)"
echo "read hadoop/etc/hadoop/slaves..."

HADOOP_HOME=/usr/local/hadoop

for HOST in `cat $HADOOP_HOME/etc/hadoop/slaves`
do
    echo "send file to ${HOST}..."
    rsync --progress -av /etc/hosts $HOST:/etc/hosts
done
