#!/bin/bash

## hadoopの保存データを消去する
echo "this program must be used by user(:hadoop)"
echo "delete hdfs(/tmp/hadoop-hadoop)"

HADOOP_HOME=/usr/local/hadoop
echo "delete localhost /tmp/hadoop-hadoop file..."
rm -rf /tmp/hadoop-hadoop

for HOST in `cat $HADOOP_HOME/etc/hadoop/slaves`
do
    echo "delete ${HOST} /tmp/hadoop-hadoop file..."
    ssh ${HOST} rm -rf /tmp/hadoop-hadoop
done
