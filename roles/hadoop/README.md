## 注意
hadoopクラスタ間で通信ができるかどうか確認する
```
ansible hadoop_cluster_test -m ping -i ~/ansible/host_vars/hosts -u hadoop
```

## 実行までにするべきこと

* 設定ファイルの書き換え (masterのみ)
** hdfs-site.xml
** mapred-site.xml
** core-site.xml
** yarn-site.xml
** slaves
* 設定ファイルの配布
* コマンドの実行
```
/usr/local/hadoop/sbin/start-dfs.sh
hdfs namenode -format
/usr/local/hadoop/sbin/yarn-daemon.sh --config /usr/local/hadoop/etc/hadoop start proxyserver
/usr/local/hadoop/sbin/start-yarn.sh
/usr/local/hadoop/sbin/mr-jobhistory-daemon.sh --config /usr/local/hadoop/etc/hadoop start historyserver
```