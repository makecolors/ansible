#! /bin/bash

# this program must be used after being distributed slaves files
echo "this program must be used by user(:hadoop)"
echo "read hadoop/etc/hadoop/slaves..."

HADOOP_HOME=/usr/local/hadoop

for HOST in `cat $HADOOP_HOME/etc/hadoop/slaves`
do
    rsync --progress -av $HADOOP_HOME/etc/hadoop/mapred-site.xml $HOST:$HADOOP_HOME/etc/hadoop
    rsync --progress -av $HADOOP_HOME/etc/hadoop/core-site.xml   $HOST:$HADOOP_HOME/etc/hadoop
    rsync --progress -av $HADOOP_HOME/etc/hadoop/hdfs-site.xml   $HOST:$HADOOP_HOME/etc/hadoop
    rsync --progress -av $HADOOP_HOME/etc/hadoop/yarn-site.xml   $HOST:$HADOOP_HOME/etc/hadoop
done
