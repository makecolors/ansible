# ansible
Python製のサーバ構成管理ツールにより各ソフトウェアのインストールを行うことができる

## 導入されるツール
下記のミドルウェアの導入が可能
### 導入されるミドルウェア
* hadoop (version: 2.7.1)
* spark (version: 1.4.1, 2.0.0-preview)
* emacs (version: 24.5)
* mysql (version: 5.6.26)
* php (version: 5.6.12)
* apache httpd server
* apr-util (version: 1.5.4)
* apr (version: 1.5.2)
* zabbix server, zabbix agentd (version: 3.0.3)

### 各ミドルウェアのビルドに必要となるツール
* jdk (version: 1.7.0.45)
* protobuf (version: 2.5.0)
* maven (version: 3.3.3)

### 共通設定
* common (パスの作成)
* yum (ymlファイル参照)

## 対象
全てのプログラムが動作する最低限のスペック
* OS: CentOS6.7
* ディスク容量: 20GB
* メモリ: 2GB

## 手順
### 前提
* 各サーバにおいて、ユーザansibleが用意されていること
* ユーザansibleはsudoを使える権限を持っていること
* 自動構築を実行するサーバにおいて、ansibleはインストール済みであること
* ansibleによる自動構築を実行するサーバは対象とするサーバ群に対しパスワードなしのSSH接続が可能であること


### 準備
* 自動構築プログラムを実行するサーバにユーザansibleとしてログインし、sourceをダウンロードする
```
cd ~
git clone git@bitbucket.org:makecolors/ansible.git

```
* クラスタの仕様に合わせて、"ansible/host_var/hosts"の設定をする
* pingモジュールを実行することで、クラスタにおける実行準備が整っていることを確認する
```
ansible <group_name> -m ping -i ~/ansible/host_vars/hosts
```
* クラスタの仕様に合わせて、"ansible/roles/***/vars/main.yml"の設定をする
* クラスタの仕様に合わせて、"ansible/main.yml"の設定をする
* ホスト名を使ってplaybookを実行するならば、"/etc/hosts"の設定をする


### 使い方
playbookを基にサーバ自動構築プログラムを実行する
```
ansible-playbook ~/ansible/main.yml -i ~/ansible/host_vars/hosts
```

## ディレクトリ構成

### ansibleリポジトリのディレクトリ構造
ansibleのディレクトリ構造は
[公式サイトのBest Practice](http://docs.ansible.com/ansible/playbooks_best_practices.html)
に従うものとする

* ansible/host_vars/hosts 構築する対象となるホスト名
* ansible/roles/*/tasks 構築するための命令
* ansible/roles/*/vars  変数等
* ansible/roles/*/src 構築に使用されるファイル群
* ansible/*.yml 大まかな命令(Playbook)

###　共通設定(playbook: common)完了後のサーバの主要なディレクトリ構成
ほとんどの操作は以下のディレクトリ内で行われる

* /usr/local/<software_name> シンボリックリンク
* /usr/local/build インストールされたソフトウェアのディレクトリ
* /usr/local/tarballs 圧縮ファイル
* /usr/local/src  ソフトウェアのソース